package co.simplon.characternote.entities;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import co.simplon.characternote.exception.NoMoreThan3Equipment;

public class FigureTestInsertEquipment {
    @Test
    void shouldInsertOnly2Equipments() throws NoMoreThan3Equipment{
        Figure figure = new Figure(1, "Kratos", "this is that");
        Equipement porte = new Equipement("porte");
        Equipement casq = new Equipement("casq");
        Equipement sword = new Equipement("sword");
        Equipement sward = new Equipement("sward");

        figure.insertEquiptment(porte);
        figure.insertEquiptment(casq);
        figure.insertEquiptment(sward);
        assertThrows(NoMoreThan3Equipment.class, () -> figure.insertEquiptment(sword));
    }
}
