package co.simplon.characternote.repo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import co.simplon.characternote.entities.Game;

@Repository
public class GameRepository {
    
    @Autowired
    private DataSource dataSource;



    //Query to find all Games
    public List<Game> findAll() {
        List<Game> list = new ArrayList<>();

        Connection connection = null;

        try {

            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM game");

            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Game game = new Game(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("genre"),
                        result.getInt("pegi"));
                list.add(game);
            }

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;

    }
    //Query to Find One Game
    public Game findById(int id){

        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM game WHERE id=?");
            stmt.setInt(1, id);

            ResultSet result= stmt.executeQuery();
            if( result.next()){
                Game game = new Game(
                    result.getInt("id"), 
                    result.getString("name"), 
                    result.getString("genre"), 
                    result.getInt("pegi") );

                    return game;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return null;

    }

    //Query to Delete a game
    public boolean deleteGame(int id){

        Connection connection= null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM game WHERE id=?");
            stmt.setInt(1, id);

            return stmt.executeUpdate() ==1;


        } catch (SQLException e) {
            e.printStackTrace();

        }finally{
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return false;
    }
    //Query to update Game
    public boolean update(Game game){
        Connection connection= null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement("UPDATE game SET name=?, genre=?, pegi=? WHERE id=?");

            stmt.setString(1, game.getName());
            stmt.setString(2, game.getGenre());
            stmt.setInt(3, game.getPegi());
            stmt.setInt(4, game.getId());

            return stmt.executeUpdate()==1;

        } catch (SQLException e) {
            e.printStackTrace();

        }finally{
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return false;
    }

    //Query to add a game
    public boolean add(Game game){
        Connection connection=null;

        try {
            connection= dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO game (name,genre,pegi) VALUES (?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, game.getName());
            stmt.setString(2, game.getGenre());
            stmt.setInt(3, game.getPegi());

            if(stmt.executeUpdate()==1){
                ResultSet rsKeys = stmt.getGeneratedKeys();
                rsKeys.next();
                game.setId(rsKeys.getInt(1));
                return true;
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return false;
    }

}
