package co.simplon.characternote.repo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import co.simplon.characternote.entities.Skill;


@Repository
public class SkillRepository {
    
    @Autowired
    private DataSource dataSource;

    public List<Skill> findAll() {
        List<Skill> skillList = new ArrayList<>(); 
        try {
            PreparedStatement stm = dataSource.getConnection()
            .prepareStatement("SELECT * FROM skill");

            ResultSet result = stm.executeQuery();

            while (result.next()) {
                Skill skill = new Skill(
                    result.getInt("id"),
                    result.getString("name"),
                    result.getInt("mana_cost"),
                    result.getString("genre")
                );

                skillList.add(skill);
            }

        } catch (SQLException e) {
            //TODO: handle exception
            e.printStackTrace();
        }
        return skillList;
    }
    public Skill findById(int id) {

        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM skill WHERE id=?");
            stmt.setInt(1, id);
            
            ResultSet result = stmt.executeQuery();
            if(result.next()) {
                Skill skill = new Skill(
                    result.getInt("id"),
                    result.getString("name"),
                    result.getInt("mana_cost"),
                    result.getString("genre"));
                return skill; 
            }
            
        } catch (SQLException e) {
            
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return null;
    }


    public boolean delete(int id) {

        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM skill WHERE id=?");
            stmt.setInt(1, id);
            
            return stmt.executeUpdate() == 1;
            
        } catch (SQLException e) {
            
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return false;
    }
    
    public boolean update(Skill skill) {

        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement("UPDATE skill SET name=?, mana_cost=?, genre=? WHERE id=?");

            stmt.setString(1, skill.getName());
            stmt.setInt(2, skill.getMana_cost());
            stmt.setString(3, skill.getGenre());
            stmt.setInt(4, skill.getId());
            
            return stmt.executeUpdate() == 1;
            
            
        } catch (SQLException e) {
            
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return false;
    }


    public boolean add(Skill skill) {

        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO skill (name,mana_cost,genre) VALUES (?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, skill.getName());
            stmt.setInt(2, skill.getMana_cost());
            stmt.setString(3, skill.getGenre());
            
            if(stmt.executeUpdate() == 1) {
                //Ici, on choppe l'id auto incrémenté et on l'assigne à notre chien
                ResultSet rsKeys = stmt.getGeneratedKeys();
                rsKeys.next();
                skill.setId(rsKeys.getInt(1));
                return true;
            }
            
            
        } catch (SQLException e) {
            
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return false;
    }

}
