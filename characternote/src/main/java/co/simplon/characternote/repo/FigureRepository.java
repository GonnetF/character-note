package co.simplon.characternote.repo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.characternote.entities.Figure;



@Repository
public class FigureRepository  {
    
    @Autowired
    private DataSource dataSource;

    public List<Figure> findAll(){
        List<Figure> characterList = new ArrayList<>();
        try {
            PreparedStatement stm = dataSource.getConnection()
            .prepareStatement("SELECT * FROM figure");


            ResultSet result =stm.executeQuery();

            while (result.next()){
                Figure chara = new Figure(
                result.getInt("id"),
                result.getString("name"),
                result.getString("background")
                
                );
                
                characterList.add(chara);
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return  characterList;
    }
    public Figure findById(Integer id) {
        PreparedStatement stm;
        try {
            /*
            *dans la commande SQL on utilise des point d'interogation pour introduire les valeur. cela nous permet avec le set
            *utilisé plus tard, d'eviter une faille de sécuritée. 
            *
            * */
            stm = dataSource.getConnection().prepareStatement("SELECT * FROM figure WHERE id = ?");
            stm.setInt(1, id);
            ResultSet result = stm.executeQuery();
            result.next();

            Figure figure = new Figure(
                result.getInt("int"),
                result.getString("name"),
                result.getString("background")
                );

            return figure;

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return null;
    }

    public void save(Figure figure) {
       
        PreparedStatement stm;
        try {
            
            stm = dataSource.getConnection()
            .prepareStatement("INSERT INTO figure VALUES(null, ?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
        
        
         //on utilise les set de preparedstatement pour eviter les injection sql
         /*
         * ici, on utilise le setstring pour passer la figure que l'on veux sauvegarder, du java, 
         * a la base de donnée. 
         * on utilise les geter et seter de l'objet pour les mettre dans le preparestatement.
         * 
         * */
         stm.setString(1, figure.getName()); 
         stm.setString(2, figure.getBg());
         

         stm.executeUpdate();
        
         System.out.println("test crea");

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
        public void update(Figure figure) {
            /*
            *même chose ici, on ne rentre pas dirrectement des valeurs dans la commande ; on utiliser les set String / setint pour ça.
            * petit plus : Preparedstatement.RETURN_GENERATED_KEYS sert a getourner automatiquement la clef primaire.
            *
            */
            try {
                PreparedStatement stm =  dataSource.getConnection()
                .prepareStatement("UPDATE figurecters SET name=?, background=?", PreparedStatement.RETURN_GENERATED_KEYS);
                stm.setString(1, figure.getName());
                stm.setString(2, figure.getBg());

           
                stm.executeUpdate();
    
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
          
        }
    
}

