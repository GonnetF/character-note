package co.simplon.characternote.entities;

import java.util.ArrayList;
import java.util.List;

import co.simplon.characternote.exception.NoMoreThan3Equipment;

public class Figure {
    private int id;
    private String name;
    private String bg;
    private List<Equipement> listeEquip = new ArrayList<>();
    
    
    
    public Figure(int id, String name, String bg){
        this.id = id;
        this.name = name;
        this.bg = bg;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBg() {
        return bg;
    }

    public void setBg(String bg) {
        this.bg = bg;
    }
    
    public String insertEquiptment(Equipement equipement) throws NoMoreThan3Equipment{
        if(listeEquip.size() > 2){
            throw new NoMoreThan3Equipment();
        }
        listeEquip.add(equipement);

        return "You have inserted an equipment";
    }


    public void insertEquiptment(String string) {
    }
}
