package co.simplon.characternote.entities;

public class Game {

        private int id;
        private String name;
        private String genre;
        private int pegi;
    
    
        
        public Game(int id, String name, String genre, int pegi) {
            this.id = id;
            this.name = name;
            this.genre = genre;
            this.pegi = pegi;
        }
    
        public int getId() {
            return id;
        }
    
        public void setId(int id) {
            this.id = id;
        }
    
        public String getName() {
            return name;
        }
    
        public void setName(String name) {
            this.name = name;
        }
    
        public String getGenre() {
            return genre;
        }
    
        public void setGenre(String genre) {
            this.genre = genre;
        }
    
        public int getPegi() {
            return pegi;
        }
    
        public void setPegi(int pegi) {
            this.pegi = pegi;
        }
    
}
