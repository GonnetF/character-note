package co.simplon.characternote.entities;

public class Equipement {

    private Integer id;
    private String name;
    private String effect;
    private Boolean isSpecial;

    Equipement(String name){
        this.name = name;
    }

    public Equipement(String name, String effect, Boolean isSpecial) {
        this.name = name;
        this.effect = effect;
        this.isSpecial = isSpecial;

    }

    public Equipement(Integer id, String name, String effect, Boolean isSpecial) {
        this.id = id;
        this.name = name;
        this.effect = effect;
        this.isSpecial = isSpecial;

    }

    public Equipement() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    public Boolean getIsSpecial() {
        return isSpecial;
    }

    public void setIsSpecial(Boolean isSpecial) {
        this.isSpecial = isSpecial;
    }
    
}
