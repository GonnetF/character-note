package co.simplon.characternote.exception;


public class NoMoreThan3Equipment extends Exception {
    public NoMoreThan3Equipment(){
        super("You can't have more than 3 of equipment");
    }
}
