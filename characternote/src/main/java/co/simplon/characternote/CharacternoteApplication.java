package co.simplon.characternote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CharacternoteApplication {

	public static void main(String[] args) {
		SpringApplication.run(CharacternoteApplication.class, args);
	}

}